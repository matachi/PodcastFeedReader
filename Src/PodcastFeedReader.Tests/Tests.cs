using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PodcastFeedReader;

namespace PodcastFeedReader.Tests
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void TestRealFeed()
        {
            var podcast = PodcastFeedReader.Parse("../../../TestData/UbuntuPodcast-170725202400.rss");

            Assert.AreEqual("Ubuntu Podcast", podcast.Title);
            Assert.AreEqual("http://ubuntupodcast.org", podcast.Link);
            Assert.IsTrue(podcast.Description.Contains("Upbeat and family-friendly show"));
            Assert.AreEqual(110, podcast.Episodes.Count);
        }

        [TestMethod]
        public void TestLibsynFeed()
        {
            var podcast = PodcastFeedReader.Parse("../../../TestData/EnPoddOmTeknik-170726165800.rss");

            Assert.AreEqual("En podd om teknik", podcast.Title);
            Assert.AreEqual("http://enpoddomteknik.se", podcast.Link);
            Assert.IsTrue(podcast.Description.Contains("Sveriges bästa teknikpodd"));
            Assert.AreEqual(115, podcast.Episodes.Count);
        }

        [TestMethod]
        public void TestFeedBurnerFeed()
        {
            var podcast = PodcastFeedReader.Parse("../../../TestData/LetsTalkBitcoin-170726171100.rss");

            Assert.AreEqual("The Let's Talk Bitcoin Network", podcast.Title);
            Assert.AreEqual("https://letstalkbitcoin.com/resources/files/images/LTBNETWORK-LOGO3.jpg", podcast.Link);
            Assert.IsTrue(podcast.Description.Contains("LTB Network"));
            Assert.AreEqual(20, podcast.Episodes.Count);
        }

        [TestMethod]
        public void TestSoundCloudFeed()
        {
            var podcast = PodcastFeedReader.Parse("../../../TestData/BeyondSynth-170726171500.rss");

            Assert.AreEqual("Beyond Synth", podcast.Title);
            Assert.AreEqual("https://www.patreon.com/beyondsynth", podcast.Link);
            Assert.IsTrue(podcast.Description.Contains("synthwave/new-retro/electronic music"));
            Assert.AreEqual(120, podcast.Episodes.Count);
        }

        [TestMethod]
        public void TestArtificialFeed()
        {
            var title = "My title";
            var link = "http://www.example.com";
            var description = "My description";
            var document = new XDocument(
                new XElement("rss",
                    new XElement("channel",
                        new XElement("title", title),
                        new XElement("link", link),
                        new XElement("description", description),
                        new XElement("item"),
                        new XElement("item"),
                        new XElement("item")
                    )
                )
            );

            var podcast = RssParser.Parse(document);

            Assert.AreEqual(title, podcast.Title);
            Assert.AreEqual(link, podcast.Link);
            Assert.AreEqual(description, podcast.Description);
            Assert.AreEqual(3, podcast.Episodes.Count);
        }

        [TestMethod]
        public void TestNoChannelNode()
        {
            var document = new XDocument(new XElement("rss"));

            Assert.ThrowsException<NoChannelException>(() => RssParser.Parse(document));
        }
    }
}
