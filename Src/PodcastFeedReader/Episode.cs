namespace PodcastFeedReader
{
    public class Episode
    {
        public string Title { get; set; }

        public string Link { get; set; }

        public string Description { get; set; }
    }
}
