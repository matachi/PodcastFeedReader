using System.Collections.Generic;

namespace PodcastFeedReader
{
    public class Podcast
    {
        public string Title { get; set; }

        public string Link { get; set; }

        public string Description { get; set; }

        public IList<Episode> Episodes { get; } = new List<Episode>();
    }
}
