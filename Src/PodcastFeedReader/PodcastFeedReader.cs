using System;
using System.Xml.Linq;

namespace PodcastFeedReader
{
    public static class PodcastFeedReader
    {
        public static Podcast Parse(string uri)
        {
            var document = XDocument.Load(uri);
            return RssParser.Parse(document);
        }
    }
}
