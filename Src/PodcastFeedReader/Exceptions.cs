namespace PodcastFeedReader
{
    [System.Serializable]
    public class PodcastFeedReaderException : System.Exception
    {
        public PodcastFeedReaderException(string message) : base(message) {}
    }

    [System.Serializable]
    public class NoChannelException : PodcastFeedReaderException
    {
        public NoChannelException() : base("The feed does not contain a channel node.") {}
    }
}
