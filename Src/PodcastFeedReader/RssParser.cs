using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace PodcastFeedReader
{
    public static class RssParser
    {
        public static Podcast Parse(XDocument document)
        {
            var channelNode = document.Root.Descendants("channel").FirstOrDefault();
            if (channelNode == null)
                throw new NoChannelException();

            var podcast = CreatePodcast(channelNode);
            var episodes = podcast.Episodes as List<Episode>;
            episodes.AddRange(CreateEpisodes(channelNode));

            return podcast;
        }

        private static Podcast CreatePodcast(XElement channelNode)
        {
            return new Podcast
            {
                Title = ElementDescendantValue(channelNode, "title"),
                Description = ElementDescendantValue(channelNode, "description"),
                Link = ElementDescendantValue(channelNode, "link")
            };
        }

        private static IEnumerable<Episode> CreateEpisodes(XElement channelNode)
        {
            return channelNode
                .Descendants("item")
                .Select(element => CreateEpisode(element));
        }

        private static Episode CreateEpisode(XElement itemNode)
        {
            return new Episode
            {
                Title = ElementDescendantValue(itemNode, "title"),
                Description = ElementDescendantValue(itemNode, "description"),
                Link = ElementDescendantValue(itemNode, "link")
            };
        }

        private static string ElementDescendantValue(XElement element,
            string descendantElementName)
        {
            return element.Descendants(
                descendantElementName).FirstOrDefault()?.Value.Trim() ?? "";
        }
    }
}
